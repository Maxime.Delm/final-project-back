terraform {
  required_providers {

    aws = {
      source  = "hashicorp/aws"
      version = "~>5.0"
    }

    ovh = {
         source = "ovh/ovh"
    }
  }
}

provider "aws" {
  region = "eu-north-1"
}

provider "ovh" {
  endpoint            = "ovh-eu"
  application_key     = var.ovh_application_key
  application_secret  = var.ovh_application_secret
  consumer_key        = var.ovh_consumer_key
}


# provider "ovh" {
#     endpoint = "ovh-eu"
#     application_key = 
#     application_secret = 
#     consumer_key = 
