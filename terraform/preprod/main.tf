terraform {

  backend "s3" {
    bucket                  = "tf-backend-morningnews-preprod"
    key                     = "state/terraform.tfstate"
    region                  = "eu-north-1"
  }
}

#Create EC2 instance
##EC2 backend
resource "aws_instance" "morningnews_back_vm" {
  ami             = "ami-0506d6d51f1916a96"
  instance_type   = "t3.micro"
  subnet_id       = aws_subnet.public_subnet.id
  security_groups = [aws_security_group.project_sg_backend.id]
  key_name        = aws_key_pair.morningnews_back_key.key_name
  # user_data       = var.script

  tags = {
    Name = "morningnews_back_vm"
  }
}

# ##EC2 frontend
# resource "aws_instance" "frontend_pre-prod" {
#   ami             = "ami-0506d6d51f1916a96"
#   count           = 1
#   instance_type   = "t3.micro"
#   subnet_id       = element(aws_subnet.public_subnets[*].id, count.index)
#   security_groups = [aws_security_group.project_sg_frontend.name]
#   key_name        = aws_key_pair.generetad_key.key_name

#   tags = {
#     Name = "frontend_pre-prod"
#   }
# }

# ===================================
# Security
# ===================================

# RSA key of size 4096 bits
resource "tls_private_key" "rsa_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# Key pair
# Create an AWS Key Pair using the generated public key
resource "aws_key_pair" "morningnews_back_key" {
  key_name   = "morningnews_back_key_pair"
  public_key = tls_private_key.rsa_key.public_key_openssh
}

# Private key
# Create a secret manager resource to store private key
resource "aws_secretsmanager_secret" "private_key_secret" {
  name = "morningnews_back_private_key"
  recovery_window_in_days = 0 # imediate deletion

  lifecycle {
    ##prevent_destroy = true
    #create_before_destroy = true  # Ensure to destroy before creating a new key
  }
}

# Private key
# Stores the key in in AWS Secrets Manager so it is secure
resource "aws_secretsmanager_secret_version" "private_key" {
  secret_id     = aws_secretsmanager_secret.private_key_secret.id
  secret_string = tls_private_key.rsa_key.private_key_pem
}

# Public key
# Stores the key in S3 for accessibility for both backend and frontend
resource "aws_s3_object" "public_key" {
  bucket               = "tf-backend-morningnews-preprod"
  key                  = "key/project_key.pub"
  content              = tls_private_key.rsa_key.public_key_openssh
  acl                  = "private"
  server_side_encryption = "AES256"
}

# ===================================
# DNS
# ===================================
resource "ovh_domain_zone_record" "sub_morningnews" {
  zone       = var.domain_name
  fieldtype  = "A"
  subdomain  = "backpp"
  ttl        = 3600
  target     = aws_instance.morningnews_back_vm.public_ip
}

# ===================================
# Ansible variables
# ===================================


resource "local_sensitive_file" "private_key" {
  content = tls_private_key.rsa_key.private_key_pem
  filename          = format("%s/%s/%s", abspath(path.root), ".ssh", "morningnews_back_key_pair.pem")
  file_permission   = "0600"
}


# Generate Ansible variables file in the group_vars folder
resource "local_file" "morningnews_vars" {
  filename = "../../ansible/${var.environment}/group_vars/morningnews_back_vm.yml"
  content = <<-EOF
    ansible_user: admin
    ansible_ssh_private_key_file:  ${local_sensitive_file.private_key.filename}
    ansible_port: 2046
    ansible_port: 22
    public_ip: "${aws_instance.morningnews_back_vm.public_ip}"
    instance_dns_zone : ${ovh_domain_zone_record.sub_morningnews.zone}
    instance_dns_subdomain: ${ovh_domain_zone_record.sub_morningnews.subdomain}
    hostname: morningnews_back_vm
    certbot_email: nojec60027@avastu.com
  EOF
}

# Generate Ansible hosts file
resource "local_file" "hosts" {
  content = <<-EOF
  [morningnews_back_vm]
  ${aws_instance.morningnews_back_vm.public_ip}
  EOF
  filename = "../../ansible/${var.environment}/hosts"
}

# Generate Ansible config file
resource "local_file" "ansible_cfg" {
  content = <<-EOF
  [defaults]
  remote_user= admin
  private_key_file=${local_sensitive_file.private_key.filename}
  host_key_checking = False
  [ssh_connection]
  # Paramètres spécifiques à la connexion SSH
  ssh_args = -o Port=2046
  EOF
  filename = "../../ansible/${var.environment}/ansible.cfg"
}