from locust import HttpUser, task

class Load_Test_Back(HttpUser):


    @task

    def first_test(self):

        self.client.get("/")
    
    @task

    def route_articles(self):

        self.client.get("/articles")

    @task

    def route_signup(self):

        self.client.post("/users/signup", json={
            "username": "testuser",
            "password": "testpassword"
        })

    @task

    def route_signin(self):

        self.client.post("/users/signin", json={
            "username": "testuser",
            "password": "testpassword"
        })

